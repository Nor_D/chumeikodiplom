﻿using ChumeikoDiplom.Models;
using ChumeikoDiplom.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChumeikoDiplom
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<ChekedModel> listBoxZone1 = new List<ChekedModel>();
        public bool HideFirst { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            List<CheckBoxListItem> items1 = new List<CheckBoxListItem>();
            items1.Add(new CheckBoxListItem(true, "home"));
            items1.Add(new CheckBoxListItem(false, "work"));
            items1.Add(new CheckBoxListItem(true, "cell"));
            lstExclude.ItemsSource = items1;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            StaticData.CheckBoxListItems = new List<CheckBoxListItem>();

            foreach (var item in lstExclude.ItemsSource)
            {
                var tmp = (CheckBoxListItem)item;
                StaticData.CheckBoxListItems.Add(tmp);
            }
            HideFirst = true;
            Result();
            //OsSelect window = new OsSelect();
            //window.Show();
            //this.Close();
        }
        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            var item = cb.DataContext;
            lstExclude.SelectedItem = item;
        }

        private void Result()
        {
            // 1
            var element =  StaticData.CheckBoxListItems.FirstOrDefault(x => x.Text == "home" && x.Checked == true);
            if(element != null)
            {
               // MessageBox.Show("Home is true");
            }

            //2
            bool include = false;
            foreach (var item in StaticData.CheckBoxListItems)
            {
                if(item.Text == "home" && item.Checked == true)
                {
                    include = true;
                }
            }

            if (include)
            {
               // MessageBox.Show("Home is true");
            }


            //3
            if (StaticData.CheckBoxListItems.Any(x => x.Text == "home" && x.Checked == true))
            {
                //MessageBox.Show("Home is true");
            }

            // саособ выборкаи

           if(SelectHttps())
            {
                MessageBox.Show("Https");
                return;
            }
            if (SelectCoocies())
            {
                MessageBox.Show("Coocies");
                return;
            }
        }

        private bool SelectHttps()
        {
            bool include = false;
            foreach (var item in StaticData.CheckBoxListItems)
            {
                if (item.Text == "home" && item.Checked == true)
                {
                    include = true;
                }
            }
            return include;
        }

        private bool SelectCoocies()
        {
            bool include = false;
            foreach (var item in StaticData.CheckBoxListItems)
            {
                if (item.Text == "work" && item.Checked == true)
                {
                    include = true;
                }
            }
            return include;
        }

    }

    public class CheckBoxListItem
    {
        public bool Checked { get; set; }
        public string Text { get; set; }

        public CheckBoxListItem(bool ch, string text)
        {
            Checked = ch;
            Text = text;
        }
    }

    
}
